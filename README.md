# Folding@home
Docker container to support [Folding@home](https://foldingathome.org)   .

## Clone
Clone via HTTPS:
```bash
git clone https://gitlab.com/steffen.ludwig/folding.git
cd folding
```

Clone via SSH:
```bash
git clone git@gitlab.com:steffen.ludwig/folding.git
cd folding
```

## Run
```
Usage: ./run.sh

Start Folding@home to support our Physics Department Student Council (University of Freiburg)

Options:
    -h, --help           Display this message
    --user USER          Set user ID (default: steffen.ludwig)
    --passkey PASSKEY    Use passkey authentication (default: 500f14ee75fba3ce500f14ee75fba3ce)
    --team TEAM          Set team ID to support another team (not recommended, default: 244687)
    --gpu                Enable GPU support
    --smp                Disable SMP support
    --power POWER        Power settings: light, medium, full (default: full)
    --ip IP              Client addresses which are allowed to connect without password (default: 172.17.0.1/172.17.0.1)
    --password PASSWORD  Set a command server password
```

## Connect
Connect to Folding@home via Web Control [http://localhost:7396](http://localhost:7396) or via Client Advanced Control on port 36330.

**Note:** You can detach from a running Docker Container using the CTRL-p CTRL-q key sequence. CTRL-c causes the container to be stopped.

# CUDA support
The flag ```--gpu``` enables the GPU support. Please make sure to fit the requirements first:
-   NVIDIA Graphics Driver 390.xx
-   CUDA 9.1
-   [NVIDIA Container Toolkit](https://github.com/NVIDIA/nvidia-docker)

Please test your system compatibility and your Docker setup with the NVIDIA System Management Interface program:
```bash
nvidia-smi
docker run --rm --gpus all nvidia/cuda:9.1-base nvidia-smi
```

## CUDA installation on Ubuntu 18.04 and Linux Mint 19
```bash
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$(. /etc/os-release; echo $ID$VERSION_ID)/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt update
sudo apt install nvidia-cuda-dev nvidia-docker2
```
