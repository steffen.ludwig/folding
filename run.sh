#!/bin/bash
#===============================================================================
# FILE:         run.sh
#
# USAGE:        ./run.sh
#
# DESCRIPTION:  Start Folding@Home
#
# OPTIONS:      ---
# REQUIREMENTS: ---
# BUGS:         ---
# NOTES:        ---
# AUTHOR:       Steffen Ludwig, gitlab@3141593.de
# VERSION:      1.8
# CREATED:      21.03.2020
# REVISION:     03.04.2020
#===============================================================================

_user=steffen.ludwig
_passkey=500f14ee75fba3ce500f14ee75fba3ce
_team=244687
_gpu=false
_smp=true
_power=full
_ip=172.17.0.1/172.17.0.1
_detached=false
_dir=$(cd $(dirname $0); pwd)

# Change directory
cd $_dir

function _help() {
    cat <<- EOF
		Usage: ./run.sh
		
		Start Folding@home to support our Physics Department Student Council (University of Freiburg)
		
		Options:
		    -h, --help           Display this message
		    --user USER          Set user ID (default: $_user)
		    --passkey PASSKEY    Use passkey authentication (default: $_passkey)
		    --team TEAM          Set team ID to support another team (not recommended, default: $_team)
		    --gpu                Enable GPU support
		    --smp                Disable SMP support
		    --power POWER        Power settings: light, medium, full (default: $_power)
		    --ip IP              Client addresses which are allowed to connect without password (default: $_ip)
		    --password PASSWORD  Set a command server password
		    --detached           Run docker container in background
		EOF
    exit
}

# Parse flags
while (( "$#" )); do
    case "$1" in
        --user)
            _user=$2
            shift 2
            ;;
        --passkey)
            _passkey=$2
            shift 2
            ;;
        --team)
            _team=$2
            shift 2
            ;;
        --gpu)
            _gpu=true
            shift
            ;;
        --smp)
            _smp=false
            shift
            ;;
        --power)
            _power=$2
            shift 2
            ;;
        --ip)
            _ip=$2
            shift 2
            ;;
        --password)
            _password=$2
            shift 2
            ;;
        --detached)
            _detached=true
            shift
            ;;
    -h | --help | *)
            _help
            exit 1
            ;;
  esac
done

if [ $_gpu = true ]; then
    cat <<- EOF
		Please make sure to fit all the requirements:
		- NVIDIA Graphics Driver 390.xx
		- CUDA 9.1
		- NVIDIA Container Toolkit (https://github.com/NVIDIA/nvidia-docker)
		
		Please test your system compatibility and your Docker setup with the NVIDIA System Management Interface program:
		- nvidia-smi
		- docker run --rm --gpus all nvidia/cuda:9.1-base nvidia-smi
        
		EOF
fi

if [ -z $_password ]
    then
        if [ $_ip = 172.17.0.1/172.17.0.1 ]
            then echo "Connect to Folding@home Web Control (http://172.17.0.1:7396) or Client Advanced Control (172.17.0.1:36330) from localhost."
            else
                echo "Connect to Folding@home Web Control or Client Advanced Control from an IP in range $_ip:"
                for _i in $(hostname -I); do
                    echo "  http://$_i:7396, $_i:36330"
                done
        fi
    else
        if [ $_ip = 172.17.0.1/172.17.0.1 ]
            then
                echo "Connect to Folding@home Web Control (http://172.17.0.1:7396) or Client Advanced Control (172.17.0.1:36330) from localhost or using the password:"
                for _i in $(hostname -I); do
                    echo "  $_i:36330"
                done
            else
                echo "Connect to Folding@home Web Control or Client Advanced Control from an IP in range $_ip or using the password:"
                for _i in $(hostname -I); do
                    echo "  http://$_i:7396, $_i:36330"
                done
        fi
fi
if [ $_detached = false ]; then
    echo "HINT: You can detach from a running Docker Container using the CTRL-p CTRL-q key sequence. CTRL-c causes the container to be stopped."
fi
echo

docker run \
    $(if [ $_detached = true ]; then echo -d; fi) -i --rm -t \
    --name folding \
    $(if [ $_gpu = true ]; then echo --gpus all; fi) \
    -p 36330:36330 \
    -p 7396:7396 \
    steffenludwig/folding \
    --allow=$_ip:7396 \
    --web-allow=$_ip:7396 \
    $(if [ ! -z $_password ];
        then
            echo --password=$_password
            echo --command-allow-no-pass=$_ip
    fi) \
    --user=$_user \
    --passkey=$_passkey \
    --team=$_team \
    --gpu=$_gpu \
    --smp=$_smp \
    --power=$_power
