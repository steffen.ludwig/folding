FROM ubuntu:18.04
MAINTAINER Steffen Ludwig

ENV CUDA_VERSION 9.1.85
ENV CUDA_PKG_VERSION 9-1=$CUDA_VERSION-1
ENV PATH /usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV NVIDIA_REQUIRE_CUDA "cuda>=9.1"

RUN \
    export DEBIAN_FRONTEND=noninteractive; \
    
    # Install software
    apt update; \
    apt upgrade -y; \
    apt install -y gnupg wget; \
    
    # Install CUDA 9.1
    wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.1.85-1_amd64.deb; \
    apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub; \
    apt install -y ./cuda-repo-ubuntu1604_9.1.85-1_amd64.deb; \
    rm ./cuda-repo-ubuntu1604_9.1.85-1_amd64.deb; \
    apt update; \
    apt install -y cuda-cudart; \
    ln -s cuda-9.1 /usr/local/cuda; \
    echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf; \
    echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf; \
    
    # Install OpenCL
    apt install -y ocl-icd-opencl-dev; \
    mkdir -p /etc/OpenCL/vendors; \
    echo "libnvidia-opencl.so.1" > /etc/OpenCL/vendors/nvidia.icd; \
    
    # Download and install client
    wget https://download.foldingathome.org/releases/public/release/fahclient/debian-stable-64bit/v7.5/latest.deb; \
    chmod +x ./latest.deb; \
    apt install ./latest.deb; \
    rm ./latest.deb; \
    
    # Add user
    useradd -m -s /bin/bash folder; \
    
    # Remove footprint
    apt purge --autoremove -y gnupg wget; \
    apt clean; \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*;

USER folder
WORKDIR /home/folder
ENTRYPOINT ["FAHClient"]
CMD ["--allow=0/0:7396", "--web-allow=0/0:7396", "--user=steffen.ludwig", "--passkey=500f14ee75fba3ce500f14ee75fba3ce","--team=244687", "--gpu=false", "--smp=true", "--power=full"]
EXPOSE 36330
EXPOSE 7396
