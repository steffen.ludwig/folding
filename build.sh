#!/bin/bash
#===============================================================================
# FILE:         build.sh
#
# USAGE:        ./build.sh
#
# DESCRIPTION:  Build Folding@Home docker image
#
# OPTIONS:      ---
# REQUIREMENTS: ---
# BUGS:         ---
# NOTES:        ---
# AUTHOR:       Steffen Ludwig, gitlab@3141593.de
# VERSION:      1.5
# CREATED:      21.03.2020
# REVISION:     03.04.2020
#===============================================================================

_dir=$(cd $(dirname $0); pwd)

# Change directory
cd $_dir

function _help() {
    cat <<- EOF
		Usage: ./build.sh
		
		Build a Folding@home docker image with CUDA support
		
		Options:
		    -h, --help  Display this message
		EOF
    exit
}

# Parse flags
while (( "$#" )); do
    case "$1" in
        -h | --help | *)
            _help
            exit 1
            ;;
  esac
done

docker build --rm -t steffenludwig/folding:latest -t steffenludwig/folding:1.5 --no-cache .
